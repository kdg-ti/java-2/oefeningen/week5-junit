package be.kdg.mandje.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Mandje {
    private List<Artikel> mandje;

    public Mandje() {
        mandje = new ArrayList<>();
    }

    public void voegToe(Artikel artikel) {
        if (!mandje.contains(artikel)) {
            mandje.add(artikel);
        }
    }

    public void verwijder(Artikel artikel) {
        mandje.remove(artikel);
    }

    public void sorteerVolgensNaam() {
        Collections.sort(mandje);
    }

    public void sorteerVolgensPrijs() {
        mandje.sort(new Comparator<Artikel>() {
            @Override
            public int compare(Artikel artikel, Artikel anderArtikel) {
                return Double.compare(artikel.getPrijs(), anderArtikel.getPrijs());
            }
        });
    }

    public void toon() {
        for (Artikel artikel : mandje) {
            System.out.format("%-16s €%4.2f\n", artikel.getNaam(), artikel.getPrijs());
        }
    }
}

