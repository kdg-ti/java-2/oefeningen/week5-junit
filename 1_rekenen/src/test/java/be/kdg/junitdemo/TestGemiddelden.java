package be.kdg.junitdemo;

import org.junit.jupiter.api.*;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Gebruik deze klasse om de 'logische' fouten uit de klasse gemidddelden te halen.
 * Aan de tests zelf mag je niets veranderen!
 */
public class TestGemiddelden {
	private Gemiddelden gemiddelden;

	@BeforeEach
	void setUp() {
		this.gemiddelden = new Gemiddelden();
	}

	@AfterEach
	void tearDown() {
		// er hoeft niks afgesloten te worden
	}

	@BeforeAll
	public static void init() {
		// er hoeft niks te gebeuren bij het laden van deze test-klasse
	}

	@AfterAll
	public static void close() {
		// er hoeft niks te gebeuren bij het afsluiten van deze test-klasse
	}

	@Test
	public void testRekenkundigGemiddeldeNormaal() {
		gemiddelden.voegGetalToe(3);
		double gemiddelde = gemiddelden.rekenkundigGemiddelde();
		assertEquals(3, gemiddelde, "gemiddelde van 3 zou 3 moeten zijn");

		gemiddelden.voegGetalToe(5);
		gemiddelde = gemiddelden.rekenkundigGemiddelde();
		assertEquals(4.0, gemiddelde, "gemiddelde van 3 en 5 zou 4 moeten zijn");

		gemiddelden.voegGetalToe(4);
		gemiddelde = gemiddelden.rekenkundigGemiddelde();
		assertEquals(4.0, gemiddelde, "gemiddelde van 3, 4 en 5 zou 4 moeten zijn");

		gemiddelden.voegGetalToe(5);
		gemiddelde = gemiddelden.rekenkundigGemiddelde();
		assertEquals(4.25, gemiddelde, "gemiddelde van 3, 4, 5 en 5 zou 4,25 moeten zijn");
	}

	@Test
	public void testRekenkundigGemiddeldeGeenGetallen() {
		assertThrows(ArithmeticException.class,
			() -> gemiddelden.rekenkundigGemiddelde(), "het gemiddelde van geen getallen zou een exception moeten gooien");
	}

	@Test
	@Timeout(1)
	public void testRekenkundigGemiddeldeEnkelNul() {
		gemiddelden.voegGetalToe(0);
		try {
			double gemiddelde = gemiddelden.rekenkundigGemiddelde();
			assertEquals(0.0, gemiddelde, "het gemiddelde van 0 zou 0 moeten zijn");
		} catch (Exception e) {
			fail("het gemiddelde van het getal 0 mag geen exception gooien", e);
		}
	}

	@Test
	public void testMeetkundigGemiddeldeNormaal() {
		gemiddelden.voegGetalToe(8);
		double gemiddelde = gemiddelden.meetkundigGemiddelde();
		assertEquals(8.0, gemiddelde, "gemiddelde van 8 zou 8 moeten zijn");

		gemiddelden.voegGetalToe(12.5);
		gemiddelde = gemiddelden.meetkundigGemiddelde();
		assertEquals(10.0, gemiddelde, "gemiddelde van 8 en 12.5 zou 10 moeten zijn");

		gemiddelden.voegGetalToe(0);
		gemiddelde = gemiddelden.meetkundigGemiddelde();
		assertEquals(0.0, gemiddelde, "gemiddelde van 0 en andere getallen moet 0 zijn");

		gemiddelden.maakLeeg();
		gemiddelden.voegGetalToe(8);
		gemiddelden.voegGetalToe(27);
		gemiddelden.voegGetalToe(343);
		gemiddelde = gemiddelden.meetkundigGemiddelde();
		assertEquals(42, gemiddelde, 0.000000001, "het gemiddelde van 8, 27 en 343 zou 42 moeten zijn");

		gemiddelden.voegGetalToe(42);
		gemiddelde = gemiddelden.meetkundigGemiddelde();
		assertEquals(42, gemiddelde, 0.0001, "het gemiddelde van 8, 27, 343 en 42 zou 42 moeten zijn");

		gemiddelden.voegGetalToe(8);
		gemiddelde = gemiddelden.meetkundigGemiddelde();
		assertEquals(
			30.145148646003363746113550141118,
			gemiddelde,
			0.000000001,
			"het gemiddelde van 8, 27, 343, 42 en 8 zou 30,145148646 moeten zijn");
	}

	@Test
	public void testMeetkundigGemiddeldeSpeciaal() {
		try {
			gemiddelden.meetkundigGemiddelde();
			fail("het gemiddelde van geen getallen zou een exception moeten gooien");
		} catch (ArithmeticException e) {
			// OK.
		}

		gemiddelden.voegGetalToe(0);
		try {
			double gemiddelde = gemiddelden.meetkundigGemiddelde();
			assertEquals( 0.0, gemiddelde, "het gemiddelde van 0 zou 0 moeten zijn");
		} catch (Exception e) {
			fail("het gemiddelde van het getal 0 mag geen exception gooien");
		}
	}
}
