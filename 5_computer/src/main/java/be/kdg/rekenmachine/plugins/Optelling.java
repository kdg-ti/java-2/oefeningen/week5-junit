package be.kdg.rekenmachine.plugins;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Optelling implements Plugin {

    public String getCommand() {
        return "+";
    }


    public double bereken(double x, double y) {
        return x + y;
    }
}
